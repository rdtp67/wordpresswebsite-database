create table web_user
(
	username	varchar(30),
	fname varchar(20) not null,
	mname varchar(20),
	lname varchar(20),
	email varchar(50) not null,
	icon varbinary(max),
	primary key(username)
);

create table web_user2
(
	username varchar(20),
	phone int,
	primary key(username, phone),
	foreign key (username) references web_user(username)
		on delete cascade
);

create table Web_user3
(
	username varchar(20),
	device varchar(30),
	primary key (username, device),
	foreign key (usernemer) references web_user(username)
		on delete cascade 
);

create table creditcard
(
	creditcardnum char(16),
	expdate date not null,
	securitycode varchar(4) not null check(securitycode.char_length() = 3 or securitycode.char_length() = 4),
	cardtype varchar(30) not null,
	cfname varchar(20) not null,
	cmname varchar(20),
	clname varchar(20) not null,
	cardstreetnum varchar(20) not null,
	cardstate char(2) not null,
	cardstreet varchar(100) not null,
	cardcity varchar(50) not null,
	cardzip varchar(20) not null,
	primary key(creditcardnum)
);

create table membership
(
	mtype	varchar(30),
	mprice	int	not null,
	numbooks int,
	mlength int not null,
	primary key(mtype)
);

create table favoriteslist
(
	title varchar(50),
	username varchar(30) not null,
	primary key(title, username),
	foreign key(username) references web_user(username)
		on delete cascade
	foreign key (title) references media(title)
		on delete cascade
);

create table media
(
	fiction char(1) not null check(fiction = 'F' or fiction = 'N'),
	msize int not null,
	title varchar(100),
	publication_date date,
	description varchar(2000),
	mpic varbinary(max),
	genre_type varchar(50) not null,
	primary key (title)
);

create table newspaper
(
	editor varchar(50) not null,
	city varchar(50) not null,
	title varchar(100),
	primary key(title),
	foreign key(title) references media(title)
		on delete cascade 
);
 
create table magizine
(
	editor varchar(50) not null,
	volume_num int,
	title varchar(100),
	primary key(title),
	foreign key(title) references media(title)
		on delete cascade 
);

create table book
(
	ISBN int,
	blength int,
	seriestitle varchar(50),
	series_num int,
	title varchar(100) not null,
	primary key(title),
	foreign key(title) references media(title)
		on delete cascade,
	unique (ISBN)
);

create table audiobook
(
	alength int,
	voice varchar(50),
	title varchar(100),
	primary key(title),
	foreign key(title) references media(title)
		on delete cascade 
);

create table mtype
(
	mtype varchar(30),
	start_date int,
	username varchar(30),
	primary key(mtype, username),
	foreign key(mtype) references membership(mtype),
	foreign key(username) references web_user(username)
		on delete cascade
);

create table checkout
(
	username varchar(30),
	title varchar(100),
	cdate int,
	primarykey (username, title),
	foreign key(username) references web_user
		on delete cascade,
	foreign key(title) references media(title)
		on delete cascade 
);

create table rating
(
	user_rate int,
	avg_rate int,
	review varchar(300),
	username varchar(30),
	title varchar(100),
	primary key(username, title),
	foreign key(username) references web_user
		on delete cascade,
	foreign key(title) references media(title)
		on delete cascade 
);

Create Table Admin
(
	ID Varchar(20),
	Rank Varchar(20),
	Login Varchar(15),
	Primary Key(ID)
);

Create Table Author
(
	Fname Varchar(20),
	Mname Varchar(20),
	Lname Varchar(20),
	Bio Varchar(200),
	title varchar(100),
	Primary Key (Fname,Lname),
	foreign key(title) references media(title)
		on delete cascade
);

Create Table Illustrator
(
	Fname Varchar(20),
	Mname Varchar(20),
	Lname Varchar(20),
	title Varchar(100),
	Primary Key(Fname,Lname),
	foreign key(title) references media(title)
		on delete cascade
);

Create Table Publisher1
(
	Website Varchar(30),
	Name Varchar(20),
	Email Varchar(30),
	Street_Number Varchar(20),
	State Char(2),
	Street_Name Varchar(20),
	City Varchar(20),
	Zip Char(5),
	title Varchar(100),
	Primary Key(Name),
	foreign key(title) references media(title)
		on delete cascade
);

create table publisher2
(
	Name varchar(20),
	phone int,
	primary key (Name, phone),
	foreign key (Name) references Publisher1(Name)
		on delete cascade 
);


